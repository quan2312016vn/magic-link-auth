import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { Request } from "express";

export const GetUser = createParamDecorator((data: any, ctx: ExecutionContext) => {
    const req: Request = ctx.switchToHttp().getRequest();

    return req.user;
});