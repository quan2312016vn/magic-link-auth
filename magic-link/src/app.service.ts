import { BadRequestException, CACHE_MANAGER, Inject, Injectable, Req, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectSendGrid, SendGridService } from '@ntegral/nestjs-sendgrid';
import { Cache } from 'cache-manager';
import { v4 as uuid } from 'uuid';

@Injectable()
export class AppService {
  constructor(
    private jwtService: JwtService,
    @InjectSendGrid() private sendGridService: SendGridService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {}

  async login(email: string, host: string) {
    const token = uuid();
    const magicLink = "https://" + host + "/magic-link?token=" + token;
    await this.cacheManager.set(token, email);

    const data = {
      to: 'quan2312016vn@gmail.com', // Change to your recipient
      from: 'quandinhnguyenminh@gmail.com', // Change to your verified sender
      subject: 'Login to MagicLink Test',
      html: `Magic link: <a href="${magicLink}">${magicLink}</a>`,
    }

    await this.sendGridService.send(data);
  }

  async verifyMagicLink(token: string): Promise<{email: string, jwtToken: string}> {
    let email = await this.cacheManager.get(token);

    if (!email) {
      throw new BadRequestException();
    }

    await this.cacheManager.del(token);

    const jwtToken = await this.jwtService.signAsync({ email });
    return {email: email as string, jwtToken};
  }
}
