import { Body, Controller, Get, Post, Query, Render, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request, Response } from "express";
import { AppService } from './app.service';
import { GetUser } from './decorators/get-user.decorator';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render("index")
  getHomePage() {}

  @Post("/login")
  async login(@Body("email") email: string, @Req() req: Request) {
    await this.appService.login(email, req.headers.host);
  }

  @Get("/magic-link")
  async verifyMagicLink(
    @Query("token") token: string,
    @Res({ passthrough: true }) res: Response
  ): Promise<string> {
    const {email, jwtToken} = await this.appService.verifyMagicLink(token);
    res.cookie("token", jwtToken);
    return "Login as " + email;
  }

  @Get("/protected")
  @UseGuards(AuthGuard())
  async getProtectedPage(@GetUser() user: { email: string }) {
    return "Hi there, " + user.email;
  }
}
