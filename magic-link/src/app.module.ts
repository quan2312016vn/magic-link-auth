import { Module, CacheModule, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { SendGridModule } from '@ntegral/nestjs-sendgrid';
import * as redisStore from 'cache-manager-redis-store';
import * as dotenv from 'dotenv';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthHeaderConvertMiddlware } from './middlewares/auth-header-convert.middleware';

dotenv.config();

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: "jwt" }),
    JwtModule.register({
      secret: "key",
      signOptions: {
        expiresIn: 3600,
      }
    }),
    SendGridModule.forRoot({
      apiKey: process.env.SENDGRID_API || "put api to your .env"
    }),
    CacheModule.register({
      store: redisStore,
      host: "redis-19107.c252.ap-southeast-1-1.ec2.cloud.redislabs.com",
      port: 19107,
      auth_pass: "Abcdef.789",
      ttl: 3600
    })
  ],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthHeaderConvertMiddlware)
      .forRoutes("*");
  }
}
